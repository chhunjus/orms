import 'cypress-file-upload';
import 'cypress-mailosaur';

Cypress.Commands.add('externalUserLogin', (email, password) => {
    const apiUrl = Cypress.env('api_url');

    var formData = new FormData();
    formData.append("grant_type", "password");
    formData.append("username", email);
    formData.append("password", password);
    formData.append("user_type", "EXTERNAL");

    var xhr = new XMLHttpRequest();
    xhr.withCredentials = true;

    xhr.addEventListener("readystatechange", function() {
        if (this.readyState === 4) {
            const b = btoa("access_token")

            const a = JSON.parse(this.response)
            const token = a.access_token

            const tokenWithBrowserData = JSON.stringify({ tkvrt: token });
            const tokenWithBrowserDataEncoded = btoa(tokenWithBrowserData);
            const tokenWithBrowserDataEncodedSplit = [tokenWithBrowserDataEncoded.substring(0, 20), tokenWithBrowserDataEncoded.substring(20)].reverse().join("");
            window.localStorage.setItem(btoa(b), tokenWithBrowserDataEncodedSplit)
        }

    });


    xhr.open("POST", apiUrl + "/login");
    xhr.setRequestHeader("Authorization", "Basic Y2xpZW50aWQ6c2VjcmV0");

    xhr.send(formData);
    cy.wait(1000)
})

Cypress.Commands.add('internalUserLogin', (email, password) => {
    const apiUrl = Cypress.env('api_url');

    var formData = new FormData();
    formData.append("grant_type", "password");
    formData.append("username", email);
    formData.append("password", password);
    formData.append("user_type", "INTERNAL");

    var xhr = new XMLHttpRequest();
    xhr.withCredentials = true;

    xhr.addEventListener("readystatechange", function() {
        if (this.readyState === 4) {
            const b = btoa("access_token")

            const a = JSON.parse(this.response)
            const token = a.access_token

            const tokenWithBrowserData = JSON.stringify({ tkvrt: token });
            const tokenWithBrowserDataEncoded = btoa(tokenWithBrowserData);
            const tokenWithBrowserDataEncodedSplit = [tokenWithBrowserDataEncoded.substring(0, 20), tokenWithBrowserDataEncoded.substring(20)].reverse().join("");
            window.localStorage.setItem(btoa(b), tokenWithBrowserDataEncodedSplit)
        }

    });


    xhr.open("POST", apiUrl + "/login");
    xhr.setRequestHeader("Authorization", "Basic Y2xpZW50aWQ6c2VjcmV0");

    xhr.send(formData);
    cy.wait(1000)
})

Cypress.Commands.add('nepToEng', () => {
    let language1 = 'En'
    let language2 = 'नेपा'
    cy.get('.header > .list > .d-flex > .btn').then(($button) => {
        if ($button.text() === language1) {
            cy.log(language1);
        } else if ($button.text() === language2) {
            cy.wrap($button).click()
            cy.get('.header > .list > .d-flex > .btn').should('have.text', language1)
        }
    })
})

Cypress.Commands.add('profileNextButton', () => {
    cy.nepToEng()
    cy.get('button[type="submit"]').contains('Next').click()
})

Cypress.Commands.add('profileSaveButton', () => {
    cy.nepToEng()
    cy.get('button[type="submit"]').contains('Save').click()
})

Cypress.Commands.add('uploadPdf', (locator, fullFilePath) => {
    cy.get(locator).attachFile({ filePath: fullFilePath, encoding: "base64" })
})

Cypress.Commands.add('selectOption', (locator, data) => {
    cy.get(locator).click().type('{backspace}')
    cy.wait(25)
    cy.get(locator).click().type(data, { delay: 10 }).then(() => {
        cy.containsExactWord('.css-ry9ur5-menu', data)
    })
})

Cypress.Commands.add('selectOptionUp', (locator, secondLocator, data) => {
    cy.get(locator).click().type('{backspace}')
    cy.wait(25)
    cy.get(locator).click().type(data, { delay: 10 }).then(() => {
        cy.containsExactWord(secondLocator, data)
    })
})

Cypress.Commands.add('containsExactWord', (locator, word) => {
    cy.get(locator).contains(new RegExp(word, "g")).click()
})

Cypress.Commands.add('addPersonalProfile', () => {
    cy.fixture('profile/personalProfile').then(personal => {
        cy.get('#firstName').clear().type(personal.firstName)
        cy.get('#lastName').clear().type(personal.lastName)
        cy.get('#firstNameNepali').clear().type(personal.firstNameNepali);
        cy.get('#lastNameNepali').clear().type(personal.lastNameNepali);
        cy.get('#dobBS').clear().type(personal.dateOfBirthBs, { force: true });
        cy.get('#birthPlace').clear().type(personal.birthPlace);
        cy.get('#fatherFirstName').clear().type(personal.fatherFirstName);
        cy.get('#fatherLastName').clear().type(personal.fatherLastName);
        //guardianPhoneNo is FatherPhoneNo. If error then it is already renamed.
        cy.get('#guardianPhoneNo').clear().type(personal.guardianPhoneNo);
        cy.get('#motherFirstName').clear().type(personal.motherFirstName);
        cy.get('#motherLastName').clear().type(personal.motherLastName);
        cy.get('#motherPhoneNo').clear().type(personal.motherPhoneNo)
        cy.get('#grandFatherFirstName').clear().type(personal.grandFatherFirstName);
        cy.get('#grandFatherLastName').clear().type(personal.grandFatherLastName);

        // first clear district and then select
        cy.get('#citizenShipNo').clear().type(personal.citizenShipNo)
        cy.get('#districtId').click().type('{backspace}')
        cy.wait(200)
        cy.get('#districtId').click().type(personal.districtNameNp + '{enter}')
        cy.get('#citizenshipIssuedDateBs').clear({ force: true }).type(personal.citizenshipIssuedDateBs);
        cy.get('#citizenshipIssuedOffice').clear().type(personal.citizenshipIssuedOffice);
        cy.selectOption('#citizenshipTypeId', personal.citizenshipTypeNp);
    })
})

Cypress.Commands.add('checkPersonalProfile', (personal) => {
    cy.get('#firstName').should('have.value', personal.firstName);
    cy.get('#lastName').should('have.value', personal.lastName);
    cy.get('#firstNameNepali').should('have.value', personal.firstNameNepali);
    cy.get('#lastNameNepali').should('have.value', personal.lastNameNepali);
    cy.get('#dobAD').should('have.value', personal.dateOfBirthAd);
    cy.get('#birthPlace').should('have.value', personal.birthPlace);
    cy.get('#fatherFirstName').should('have.value', personal.fatherFirstName);
    cy.get('#fatherLastName').should('have.value', personal.fatherLastName);
    cy.get('#guardianPhoneNo').should('have.value', personal.guardianPhoneNo);
    cy.get('#motherFirstName').should('have.value', personal.motherFirstName);
    cy.get('#motherLastName').should('have.value', personal.motherLastName);
    cy.get('#motherPhoneNo').should('have.value', personal.motherPhoneNo);
    cy.get('#grandFatherFirstName').should('have.value', personal.grandFatherFirstName);
    cy.get('#grandFatherLastName').should('have.value', personal.grandFatherLastName);
    cy.get('#districtId').eq(0).should('have.text', 'काठमाडौँ');
    cy.get('#citizenshipIssuedDateAd').should('have.value', personal.citizenshipIssuedDateAd);
    cy.get('#citizenshipIssuedOffice').should('have.value', personal.citizenshipIssuedOffice);
    // cy.get('#citizenshipTypeId').should('have.text', personal.citizenshipTypeEn);
})

Cypress.Commands.add('addContact', () => {
    cy.fixture('profile/contact').then((contactData) => {
        const contact = contactData.data[1]
        const contactcitizenship = contactData.data[0]
        cy.selectOption('#permastateId', contact.stateNameNp)
        cy.selectOption('#permadistrictId', contact.districtNameNp)
        cy.selectOption('#permalocalBodyId', contact.localBodyNameNp)
        cy.get('#permawardNo').clear().type(contact.wardNo);
        cy.get('#permamarga').clear().type(contact.marga);
        cy.get('#permatoleName').clear().type(contact.toleName);
        cy.get('#permamobileNo').clear().type(contact.mobileNo);
        cy.get('#isPermanent').check();
        cy.selectOption('#zoneId', contactcitizenship.zoneNameNp)
        cy.selectOption('#districtId', contactcitizenship.districtNameNp)
        cy.get('#citizenshipLocalBody').clear().type(contactcitizenship.citizenshipLocalBody);
        cy.get('#wardNo').clear().type(contactcitizenship.wardNo);
        cy.get('#permamailingAddress').clear().type(contact.mailingAddress)
    })
})

Cypress.Commands.add('checkContact', (contactData) => {
    const contact = contactData.data[1]
    const contactcitizenship = contactData.data[0]
    cy.get('#permastateId').should('have.text', contact.stateNameNp);
    cy.get('#permadistrictId').should('have.text', contact.districtNameNp)
    cy.get('#permalocalBodyId').should('have.text', contact.localBodyNameNp)
    cy.get('#permawardNo').should('have.value', contact.wardNo);
    cy.get('#permamarga').should('have.value', contact.marga);
    cy.get('#permatoleName').should('have.value', contact.toleName);
    cy.get('#permamobileNo').should('have.value', contact.mobileNo);
    cy.get('#zoneId').should('have.text', contactcitizenship.zoneNameNp)
    cy.get('#districtId').should('have.text', contactcitizenship.districtNameNp)
    cy.get('#citizenshipLocalBody').should('have.value', contactcitizenship.citizenshipLocalBody);
    cy.get('#wardNo').should('have.value', contactcitizenship.wardNo);
    cy.get('#permamailingAddress').should('have.value', contact.mailingAddress)
})

Cypress.Commands.add('addExtraDetails', () => {
    cy.fixture('profile/extra-details').then((extraDetails) => {
        //cannot use .select() because it can be called only on <select>. Here the ids are not <select>. They are react classes. 
        cy.selectOption('#casteId', extraDetails.casteName)
        cy.get('#religionId').click().type('{backspace}')
            //Coulnot select religion
        cy.get('#religionId').click().type(extraDetails.religion + '{enter}')
            // cy.selectOption('#religionId', extraDetails.religion)
        if (extraDetails.maritalStatus == 'UnMarried') {
            cy.get('#unMarried').check();
        } else {
            cy.get('#married').check();

        }
        cy.get('#employmentStatusOther').clear().type(extraDetails.employmentStatusOther)
        cy.selectOption('#motherTongueId', extraDetails.motherTongueName)

        //father education
        cy.get('#fatherEducation').click().type('{backspace}')
        cy.wait(100)
        cy.get('#fatherEducation').click().type(extraDetails.fatherEducation, { delay: 100 }).then(() => {
            cy.containsExactWord('.css-ry9ur5-menu', extraDetails.fatherEducation)
        })
        cy.selectOption('#fatherEducationLevel', extraDetails.fatherEducationLevel)
        cy.selectOption('#fatherOccupation', extraDetails.fatherOccupation)
            //mother education
        cy.get('#motherEducation').click().type('{backspace}')
        cy.wait(100)
        cy.get('#motherEducation').click().type(extraDetails.motherEducation, { delay: 100 }).then(() => {
            cy.containsExactWord('.css-ry9ur5-menu', extraDetails.motherEducation)
        })

        cy.selectOption('#motherEducationLevel', extraDetails.motherEducationLevel)
        cy.selectOption('#motherOccupation', extraDetails.motherOccupation)

        if (extraDetails.area == 'hill') {
            cy.get('#hill').check();
        } else if (extraDetails.area == 'mountain') {
            cy.get('#mountain').check();
        } else if (extraDetails.area == 'terai') {
            cy.get('#terai').check();
        }
    })
})

Cypress.Commands.add('checkExtraDetails', (extraDetails) => {
    cy.get('#casteId').should('have.text', extraDetails.casteName)
    cy.get('#religionId').within(() => {
        cy.get('input[name="religionId"]').should('have.value', extraDetails.religionId)
    })
    if (extraDetails.maritalStatus == 'UnMarried') {
        cy.get('#unMarried').should('be.checked');
    } else {
        cy.get('#married').should('be.checked');

    }
    cy.get('#employmentStatusOther').should('have.value', extraDetails.employmentStatusOther)
    cy.get('#motherTongueId').within(() => {
        cy.get('input[name="motherTongueId"]').should('have.value', extraDetails.motherTongueId)
    })

    //father education
    cy.get('#fatherEducation').should('have.text', extraDetails.fatherEducation)
    cy.get('#fatherEducationLevel').should('have.text', extraDetails.fatherEducationLevel)
    cy.get('#fatherOccupation').should('have.text', extraDetails.fatherOccupation)
        //mother education
    cy.get('#motherEducation').should('have.text', extraDetails.motherEducation)
    cy.get('#motherEducationLevel').should('have.text', extraDetails.motherEducationLevel)
    cy.get('#motherOccupation').should('have.text', extraDetails.motherOccupation)

    if (extraDetails.area == 'hill') {
        cy.get('#hill').should('be.checked');
    } else if (extraDetails.area == 'mountain') {
        cy.get('#mountain').should('be.checked');
    } else if (extraDetails.area == 'terai') {
        cy.get('#terai').should('be.checked');
    }
})

Cypress.Commands.add('deleteEducationDetails', () => {
    const apiUrl = Cypress.env('api_url');

    //first all education is deleted, then only education is added.
    let educationData, educationDataLength;
    cy.intercept('GET', apiUrl + '/external/education-details').as('getEducationDetails')
    cy.visit('/user/profile/education')
    cy.nepToEng()
    cy.wait('@getEducationDetails').then(resp => {
            educationData = resp.response.body.data
            educationDataLength = educationData.length
                //if education data is not empty, then it deletes all the education.
            if (educationDataLength > 0) {
                for (let i = 0; i < educationDataLength; i++) {
                    cy.get('.ic-delete').eq(0).click()
                    cy.get('.btn-group > .btn-icon').contains('Delete').click()
                }
            }
        })
        //Must confirm that there is no data in Education.
    cy.get('.table-responsive > .des').should('have.text', 'No data available')
})

Cypress.Commands.add('addEducationDetails', () => {
    cy.fixture('profile/single-education-detail').then(body => {
        const educationDetails = body.data[0]
        cy.get('button[type="button"].btn-icon').click()
        cy.wait(500)
        cy.get('#level').click().type(educationDetails.educationLevelNameEn + '{enter}', { delay: 10 })
        cy.get('#universityOrBoard').click().type(educationDetails.boardNameEn, { delay: 10 }).then(() => {
            cy.containsExactWord('.css-ry9ur5-menu', educationDetails.boardNameEn)
        })
        cy.get('#faculty').click().type(educationDetails.facultyNameEn, { delay: 10 }).then(() => {
            cy.containsExactWord('.css-ry9ur5-menu', educationDetails.facultyNameEn)
        })
        cy.get('#course').click().type(educationDetails.courseNameEn, { delay: 10 }).then(() => {
            cy.containsExactWord('.css-ry9ur5-menu', educationDetails.courseNameEn)
        })
        cy.get('#countryId').click().type(educationDetails.countryNameEn, { delay: 10 })
        cy.get('#percentage').type(educationDetails.percentage)
        cy.get('#division').type(educationDetails.division)
        if (educationDetails.educationType == "Government") {
            cy.get('input[type="radio"][value="government"]').check()
        } else {
            cy.get('input[type="radio"][value="non-Government"]').check()
        }
        cy.get('.form-group').contains('Certificate Issued Date').next().type(educationDetails.passedDateBs)
        cy.get('input[name="passedYear"]').type(educationDetails.passedYear)
        cy.uploadPdf('#transcript', 'profile/slc_marksheet_certificate.pdf')
        cy.uploadPdf('#characterCertificate', 'profile/slc_character_certificate.pdf')
        cy.uploadPdf('#provisionalCertificate', 'profile/slc_provisional_certificate.pdf')
    })
})

Cypress.Commands.add('checkEducationDetails', (education) => {
    const educationDetail = education.data[0]
    cy.get('tbody > tr > :nth-child(1)').should('have.text', educationDetail.educationLevelNameEn);
    cy.get('tbody > tr > :nth-child(2)').should('have.text', educationDetail.boardNameEn);
    cy.get('tbody > tr > :nth-child(3)').should('have.text', educationDetail.facultyNameEn);
    cy.get('tbody > tr > :nth-child(4)').should('have.text', educationDetail.courseNameEn);
    cy.get('tbody > tr > :nth-child(5)').should('have.text', educationDetail.division);
    cy.get('tbody > tr > :nth-child(6)').should('have.text', educationDetail.percentage);
    const text = "Government"
    if (educationDetail.educationType == "government") {
        educationDetail.educationType = text
        cy.get('tbody > tr > :nth-child(7)').should('have.text', educationDetail.educationType);
    }

    cy.get('tbody > tr > :nth-child(8)').should('have.text', educationDetail.passedDateBs);
    cy.get('tbody > tr > :nth-child(9)').should('have.text', educationDetail.passedDateAd);
    cy.get('tbody > tr > :nth-child(10)').should('have.text', educationDetail.passedYear);
})

Cypress.Commands.add('deleteTrainingDetails', () => {
    const apiUrl = Cypress.env('api_url');

    //first all training is deleted, then only training is added.
    let trainingData, trainingDataLength;
    cy.intercept('GET', apiUrl + '/external/training-details').as('getTrainingDetails')
    cy.visit('/user/profile/training')
    cy.nepToEng()
    cy.wait('@getTrainingDetails').then(resp => {
            trainingData = resp.response.body.data
            trainingDataLength = trainingData.length
                //if education data is not empty, then it deletes all the education.
            if (trainingDataLength > 0) {
                for (let i = 0; i < trainingDataLength; i++) {
                    cy.get('.ic-delete').eq(0).click()
                    cy.get('.btn-group > .btn-icon').contains('Delete').click()
                }
            }
        })
        //Must confirm that there is no data in Education.
    cy.get('.table-responsive > .des').should('have.text', 'No data available')
})

Cypress.Commands.add('addTrainingDetails', () => {
    cy.fixture('profile/single-training-detail').then(body => {
        const trainingDetails = body.data[0]
        cy.get('button[type="button"].btn-icon').click()
        cy.get('.form-control[type="text"][name="instituteName"]').type(trainingDetails.instituteName, { delay: 10 })
        cy.get('.form-control[type="text"][name="trainingName"]').type(trainingDetails.trainingName, { delay: 10 })
        cy.get('#trainingTypeId').type(trainingDetails.typeNameEn + '{enter}', { delay: 10 })
        cy.get('.form-control[type="text"][name="trainingFromBs"]').type(trainingDetails.trainingFromBs, { delay: 10 })
        cy.get('.form-control[type="text"][name="trainingToBs"]').type(trainingDetails.trainingToBs, { delay: 10 })
        cy.uploadPdf('#trainingCertificate', 'profile/training_certificate.pdf')
    })
})

Cypress.Commands.add('checkTrainingDetails', (training) => {
    const trainingDetails = training.data[0]
    cy.get('tbody > tr > :nth-child(1)').should('have.text', trainingDetails.instituteName);
    cy.get('tbody > tr > :nth-child(2)').should('have.text', trainingDetails.trainingName);
    cy.get('tbody > tr > :nth-child(3)').should('have.text', trainingDetails.typeNameEn);
    cy.get('tbody > tr > :nth-child(4)').should('have.text', trainingDetails.trainingFromAd);
    cy.get('tbody > tr > :nth-child(5)').should('have.text', trainingDetails.trainingFromBs);
    cy.get('tbody > tr > :nth-child(6)').should('have.text', trainingDetails.trainingToAd);
    cy.get('tbody > tr > :nth-child(7)').should('have.text', trainingDetails.trainingToBs);
})

Cypress.Commands.add('deleteProfessionalCouncilDetails', () => {
    const apiUrl = Cypress.env('api_url');

    //first all training is deleted, then only training is added.
    let professionalCouncilData, professionalCouncilDataLength;
    cy.intercept('GET', apiUrl + '/external/professional-council').as('getprofessionalCouncilDetails')
    cy.visit('/user/profile/professional')
    cy.nepToEng()
    cy.wait('@getprofessionalCouncilDetails').then(resp => {
            professionalCouncilData = resp.response.body.data
            professionalCouncilDataLength = professionalCouncilData.length
                //if education data is not empty, then it deletes all the education.
            if (professionalCouncilDataLength > 0) {
                for (let i = 0; i < professionalCouncilDataLength; i++) {
                    cy.get('.ic-delete').eq(0).click()
                    cy.get('.btn-group > .btn-icon').contains('Delete').click()
                }
            }
        })
        //Must confirm that there is no data in Education.
    cy.get('.table-responsive > .des').should('have.text', 'No data available')
})

Cypress.Commands.add('addProfessionalCouncilDetails', () => {
    cy.fixture('profile/single-professionalCouncil-detail').then(body => {
        const professionalCouncilDetails = body.data[0]
        cy.get('button[type="button"].btn-icon').click()
        cy.get('#councilId').type(professionalCouncilDetails.councilNameEn + '{enter}', { delay: 10 })
        if (professionalCouncilDetails.degreeType == "permanent") {
            cy.get('input[name="degreeType"][type="radio"][value="permanent"]').check()
        } else if (professionalCouncilDetails.degreeType == "temporary") {
            cy.get('input[name="degreeType"][type="radio"][value="temporary"]').check()
        } else if (professionalCouncilDetails.degreeType == "sport") {
            cy.get('input[name="degreeType"][type="radio"][value="sport"]').check()
        }
        cy.get('.form-control[type="text"][name="dartaNo"]').type(professionalCouncilDetails.dartaNo, { delay: 10 })
        cy.get('.form-control[type="text"][name="issuedDateBs"]').type(professionalCouncilDetails.issuedDateBs, { delay: 10 })
        cy.get('.form-control[type="text"][name="renewDateBs"]').type(professionalCouncilDetails.renewDateBs, { delay: 10 })
        cy.get('.form-control[type="text"][name="validityDateBs"]').type(professionalCouncilDetails.validityDateBs, { delay: 10 })
        cy.uploadPdf('#certificateFrontSide', 'profile/training_certificate.pdf')
        cy.uploadPdf('#certificateBackSide', 'profile/training_certificate.pdf')
    })
})

Cypress.Commands.add('checkProfessionalCouncilDetails', (professionalCouncil) => {
    const professionalCouncilDetail = professionalCouncil.data[0]
    cy.get('tbody > tr > :nth-child(1)').should('have.text', professionalCouncilDetail.councilNameEn);
    cy.get('tbody > tr > :nth-child(2)').should('have.text', professionalCouncilDetail.degreeType);
    //3 is cancelled because it is sport and there is no adding of sport in test.
    cy.get('tbody > tr > :nth-child(4)').should('have.text', professionalCouncilDetail.dartaNo);
    cy.get('tbody > tr > :nth-child(5)').should('have.text', professionalCouncilDetail.issuedDateAd);
    cy.get('tbody > tr > :nth-child(6)').should('have.text', professionalCouncilDetail.issuedDateBs);
    cy.get('tbody > tr > :nth-child(7)').should('have.text', professionalCouncilDetail.renewDateAd);
    cy.get('tbody > tr > :nth-child(8)').should('have.text', professionalCouncilDetail.renewDateBs);
    cy.get('tbody > tr > :nth-child(9)').should('have.text', professionalCouncilDetail.validityDateAd);
    cy.get('tbody > tr > :nth-child(10)').should('have.text', professionalCouncilDetail.validityDateBs);
})


Cypress.Commands.add('deleteReferenceDetails', () => {
    const apiUrl = Cypress.env('api_url');

    //first all reference is deleted, then only reference is added.
    let referenceData, referenceDataLength;
    cy.intercept('GET', apiUrl + '/external/contact-person').as('getReferenceDetails')
    cy.visit('/user/profile/reference')
    cy.wait(1000)
    cy.nepToEng()
    cy.wait('@getReferenceDetails').then(resp => {
            referenceData = resp.response.body.data
            referenceDataLength = referenceData.length
                //if reference data is not empty, then it deletes all the reference.
            if (referenceDataLength > 0) {
                for (let i = 0; i < referenceDataLength; i++) {
                    cy.get('.ic-delete').eq(0).click()
                    cy.get('.btn-group > .btn-icon').contains('Delete').click()
                    cy.wait('@getReferenceDetails')
                }
            }
        })
        //Must confirm that there is no data in Reference.
    cy.get('.table-responsive > .des').should('have.text', 'No data available')
})

//the fixture file contains json with 2 index.
Cypress.Commands.add('addReferenceDetails', (reference, index) => {
    cy.fixture('profile/' + reference).then(body => {
        const getReferenceDetails = body.data[index]
        cy.get('#name').type(getReferenceDetails.name)
        cy.get('#address').type(getReferenceDetails.address)
        cy.get('#currentPost').type(getReferenceDetails.currentPost)
        cy.get('#mobileNumber').type(getReferenceDetails.mobileNumber)
    })
})

Cypress.Commands.add('checkReferenceDetails', (reference) => {
    // cy.log(reference.data[0].name)
    for (let i = 0; i < 2; i++) {
        let referenceData = reference.data[i]
        let rowIndex = i + 1
        cy.get('tbody > :nth-child(' + rowIndex + ') > :nth-child(1)').should('have.text', referenceData.name);
        cy.get('tbody > :nth-child(' + rowIndex + ') > :nth-child(2)').should('have.text', referenceData.address);
        cy.get('tbody > :nth-child(' + rowIndex + ') > :nth-child(3)').should('have.text', referenceData.currentPost);
        cy.get('tbody > :nth-child(' + rowIndex + ') > :nth-child(4)').should('have.text', referenceData.mobileNumber);
    }
})