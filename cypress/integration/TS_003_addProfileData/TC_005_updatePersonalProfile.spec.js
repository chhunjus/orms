//remove skip only when you want to add data.
describe('Updates Personal Profile', () => {
    const apiUrl = Cypress.env('api_url');

    //use only when data needed to be updated, create new fixture and enter username and password there.
    beforeEach('logins', () => {
        cy.readFile("cypress/fixtures/login.json", (err, data) => {
            if (err) {
                return console.error(err);
            };
        }).then(data => {
            cy.externalUserLogin(data.email, data.password)
        })
    })


    it('updates personal profile', () => {
        cy.visit('/user/profile/personal')
        cy.addPersonalProfile()
        cy.intercept('POST', apiUrl + '/external/personal-details').as('postPersonalDetails')
        cy.profileNextButton()
            //gets the POST response and asserts against the input fields
        cy.wait('@postPersonalDetails').then(response => {
            const resp = response.response.body.data
            cy.visit('/user/profile/personal')
            cy.checkPersonalProfile(resp)
        })
    })


    it('updates contact', () => {
        cy.intercept('GET', apiUrl + '/external/address-details').as('getAddressDetails')
        cy.visit('/user/profile/contact')
        cy.wait(1000)
            // first clear pradesh and then select
        cy.addContact()
        cy.intercept('POST', apiUrl + '/external/address-details').as('postAddressDetails')
        cy.profileNextButton()
            // //gets the POST response and asserts against the input fields
            //create assertion remaining
        cy.wait('@postAddressDetails')
        cy.visit('/user/profile/contact')
        cy.wait('@getAddressDetails')
            .then(response => {
                const contact = response.response.body
                cy.checkContact(contact)
            })
    })

    it('updates extra-details', () => {
        cy.intercept('GET', apiUrl + '/external/extra-details').as('getExtraDetails')
        cy.visit('/user/profile/extra')
        cy.wait(1000)
        cy.nepToEng()
        cy.addExtraDetails()
        cy.intercept('POST', apiUrl + '/external/extra-details').as('postExtraDetails')
        cy.profileNextButton()
            //gets the POST response and asserts against the input fields
        cy.wait('@postExtraDetails')
        cy.visit('/user/profile/extra')
        cy.wait('@getExtraDetails')
            .then(response => {
                const extraDetail = response.response.body.data
                cy.checkExtraDetails(extraDetail)
            })
    })

    //no id given to division input field.
    it('updates education-details', () => {
        cy.deleteEducationDetails()
            //Add Education
        cy.addEducationDetails()
        cy.intercept('POST', apiUrl + '/external/education-details').as('postEducationDetails')
        cy.intercept('GET', apiUrl + '/external/education-details').as('getEducationDetail')
        cy.profileSaveButton()
            //gets the POST response and asserts against the input fields
        cy.wait('@postEducationDetails')
        cy.wait('@getEducationDetail').then(resp => {
            const educationData = resp.response.body.data
            expect(educationData.length).to.eq(1)
            const education = resp.response.body
            cy.checkEducationDetails(education)
        })
    })

    //it saves the same data twice, same post api is running twice. Issue.
    it.skip('updates training', () => {
        cy.deleteTrainingDetails()
        cy.addTrainingDetails()
            // cy.intercept('POST', apiUrl + '/external/training-details')
        cy.intercept('GET', apiUrl + '/external/training-details').as('getTrainingDetail')
        cy.profileSaveButton()

        cy.wait('@getTrainingDetail').then((resp) => {
            const trainingData = resp.response.body.data
            expect(trainingData.length).to.eq(1)
            const training = resp.response.body
            cy.checkTrainingDetails(training)
        })
    })

    it('updates professional council', () => {
        cy.deleteProfessionalCouncilDetails()
        cy.addProfessionalCouncilDetails()
        cy.intercept('POST', apiUrl + '/external/professional-council').as('postProfessionalCouncilDetail')
        cy.intercept('GET', apiUrl + '/external/professional-council').as('getProfessionalCouncilDetail')
        cy.profileSaveButton()
        cy.wait('@postProfessionalCouncilDetail')
        cy.wait('@getProfessionalCouncilDetail').then((resp) => {
            const professionalData = resp.response.body.data
            expect(professionalData.length).to.eq(1)
            const professionalCouncil = resp.response.body
            cy.checkProfessionalCouncilDetails(professionalCouncil)
        })
    })

    it('updates reference', () => {
        function add(dataIndex) {
            cy.addReferenceDetails('reference', dataIndex)
            cy.profileSaveButton()
        }
        cy.deleteReferenceDetails()
        cy.intercept('GET', apiUrl + '/external/contact-person').as('referenceDetails')
        add(0)
        cy.wait('@referenceDetails').then(() => {
            add(1)
            cy.wait('@referenceDetails').then((data) => {
                cy.checkReferenceDetails(data.response.body)
            })
        })
    })

})