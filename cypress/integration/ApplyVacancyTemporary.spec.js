describe.skip('Updates Personal Profile', () => {
    const apiUrl = Cypress.env('api_url');

    //use only when data needed to be updated, create new fixture and enter username and password there.
    beforeEach('logins', () => {
        cy.readFile("cypress/fixtures/login.json", (err, data) => {
            if (err) {
                return console.error(err);
            };
        }).then(data => {
            cy.externalUserLogin(data.email, data.password)
        })
    })

    it('applies vacancy', () => {
        cy.visit('/')
            // cy.wait(1000)
        cy.nepToEng()
        cy.get('.card-vacancy').eq(0).within(() => {
            cy.get('h5').contains('VAC-02')
            cy.get('.btn-primary').eq(0).contains('Apply').click()
        })
        cy.get('.flex-center-between > .btn').contains('Apply').click({ force: true })
        cy.wait(200)
        cy.get('[type="checkbox"]').last().check()
        cy.get('[type="checkbox"]').first().check()
        cy.selectOption('#permastateId', '२ नं. प्रदेश')
        cy.get('#collectionCenterId').click().type('{backspace}')
        cy.wait(25)
        cy.selectOption('#collectionCenterId', '2 Number State Police Training Centre')
        cy.get('.footer-btn > .btn').contains('Apply').click()
        cy.get('.flex-end > .btn').contains('Select All').click()
        cy.get('.modal-footer > .btn-primary').contains('Next').click()
        cy.get('.btn-primary').contains('Yes, Confirm').click()
        cy.get('.btn-group > .btn').contains('OK').click()
        cy.wait(1000)
        cy.visit('/application')
        cy.wait(500)
        cy.get('.btn').contains('Pay').click()
        cy.wait(500)
        cy.get('#elCheckbox').check()
        cy.get('span > img').click()
        cy.get(':nth-child(2) > .form-check-label > .form-check-input').click()
    })

})