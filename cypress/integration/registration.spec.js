// describe('Registration Process', () => {
//     let id = 15;
//     for (let i = 0; i < id; i++) {

//to test registration, remove the master describe and remove ${i} from testEmail. 
//need to create function that generates new name on every test run.
describe.skip('Registration through API', () => {
        let temporaryPassword
        const apiUrl = Cypress.env('api_url');
        const serverId = 'zfpmgana'; // Replace SERVER_ID with an actual Mailosaur Server ID
        //always enter lowercase email
        const testEmail = `tester1234@${serverId}.mailosaur.net`
        const validPassword = 'Test@123'

        function En() {
            cy.get('.select').select('en');
        }

        it('Registers and gets Temporary password', () => {
            let passwordBody
            let passwordBodyIndexCount
            cy.mailosaurDeleteAllMessages(serverId)
            cy.request({
                method: 'POST',
                url: apiUrl + '/external/user/save', // baseUrl is prepended to url
                body: {
                    "firstNameEn": "Hello",
                    "middleNameEn": "",
                    "lastNameEn": "Hello",
                    "email": testEmail,
                    "phoneNumber": "9813281462"
                },
            })
            cy.wait(2000)
            cy.mailosaurGetMessage(serverId, {
                sentTo: testEmail
            }).then(email => {
                expect(email.subject).to.equal('User Registration Successful');
                passwordBody = `${email.html.body}`
                    //here 88 is added since it is distance from 'Password' to real Password, the index is: 
                passwordBodyIndexCount = passwordBody.lastIndexOf('Password') + 164
                temporaryPassword = passwordBody.substring(passwordBodyIndexCount, passwordBodyIndexCount + 8)
                cy.log(temporaryPassword)
            })
        })

        it('Logins and set up public user', () => {
            cy.visit('/login')
            En()
            cy.intercept('POST', apiUrl + '/login').as('login')
            cy.get('#username').type(testEmail)
            cy.get('#password').type(temporaryPassword)
            cy.get('.btn-primary').click()
            cy.wait('@login')
            cy.get('#newPassword').type(validPassword)
            cy.get('#confrimPassword').type(validPassword)
            cy.get('.btn').contains('Submit').click()
            cy.get('#username').type(testEmail)
            cy.get('#password').type(validPassword)
            cy.get('.btn-primary').click()
            cy.wait('@login')

            //setup public user
            cy.get('#userType-P').check()
            cy.get('input[name="dateOfBirthBs"]').type('2055-06-09')
            cy.get('.title-01').click()
            cy.get('#gender-M').check()
            cy.get('.btn').contains('Submit').click()
            cy.get('.username').should('have.text', testEmail)

        })

    })
    //     }
    // })