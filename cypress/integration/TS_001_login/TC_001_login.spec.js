/// <reference types="cypress" />

describe('Login Using Email', () => {
    const apiUrl = Cypress.env('api_url');
    const invalidEmail = 'asdfdsf'
    const wrongPassword = 'qqqqqq'
    let email, password;

    beforeEach('Visits Login Page', () => {
        cy.fixture('login').then((data) => {
            email = data.email
            password = data.password
        })
        cy.visit('/login')
        cy.wait(500)
    })

    it('disables login button when there is no data or only one field contains data', () => {
        cy.get('.btn-primary').should('be.disabled')
        cy.get('#username').type(email)
        cy.get('.btn-primary').should('be.disabled')
        cy.get('#username').clear()
        cy.get('#password').type(wrongPassword)
        cy.get('.btn-primary').should('be.disabled')
    })

    //needs to be fixed by dev
    it('gives error or invalid email', () => {
        cy.get('#username').type(invalidEmail)
        cy.get('.btn-primary').should('be.disabled')
    })

    it('unsucessful login on invalid credential', () => {
        cy.intercept(apiUrl + "/login").as('login')
        cy.get('#username').type(email)
        cy.get('#password').type(wrongPassword)
        cy.get('.btn-primary').click()
        cy.wait('@login')
        cy.get('.toast--message').should('be.visible').and('have.text', 'Bad credentials')
    })

    it('verifies password show and hide icon', () => {
        cy.get('#password').type(wrongPassword)
        cy.get('.ic-hide').click()
        cy.get('.ic-hide').should('not.exist')
        cy.get('.ic-show').click()
        cy.get('.ic-show').should('not.exist')
    })

    it('verifies language change', () => {
        cy.get('.select').select('en');
        cy.get('h4').should('have.text', 'Login')
        cy.get('.select').select('नेपा')
        cy.get('h4').should('have.text', 'लग-इन')
    })

    it('verifies public portal link', () => {
        cy.get('.select').select('en')
        cy.get('a.text-primary[role=button]').should('have.text', 'Public Portal')
        cy.get('.select').select('नेपा')
        cy.get('a.text-primary[role=button]').should('have.text', 'सार्वजनिक पोर्टल').click()
        cy.get('img').eq(0).should('be.visible')
            // cy.url().should('eq', Cypress.config().baseUrl + '/public-portal')
    })

    it('logins on valid credentials', () => {
        cy.intercept('POST', apiUrl + '/login').as('login')
        cy.readFile("cypress/fixtures/login.json", (err, data) => {
            if (err) {
                return console.error(err);
            };
        }).then(data => {
            cy.get('#username').type(data.email)
            cy.get('#password').type(data.password)
            cy.get('.btn-primary').click()
            cy.wait('@login')
            cy.get('.username').should('have.text', data.email)
        })
    })


})