describe('Login without UI', () => {
    beforeEach('loads data', () => {
        cy.readFile("cypress/fixtures/loginPMIS.json", (err, data) => {
            if (err) {
                return console.error(err);
            };
        }).then(data => {
            cy.internalUserLogin(data.pmis, data.password)
        })
    })

    it('Bypass Login', () => {
        cy.visit('/')
        cy.fixture('loginPMIS').then(resp => {
            cy.get('.username').should('have.text', resp.pmis)
        })
    })
})