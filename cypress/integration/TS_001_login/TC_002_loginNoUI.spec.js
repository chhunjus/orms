describe('Login without UI', () => {
    beforeEach('loads data', () => {
        cy.readFile("cypress/fixtures/login.json", (err, data) => {
            if (err) {
                return console.error(err);
            };
        }).then(data => {
            cy.externalUserLogin(data.email, data.password)
        })
    })

    it('Bypass Login', () => {
        cy.visit('/')
        cy.fixture('login').then(resp => {
            cy.get('.username').should('have.text', resp.email)
        })
    })
})