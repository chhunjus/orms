/// <reference types="cypress" />

Cypress.Commands.add('nepToEngAdminLogin', () => {
    let language1 = 'EN'
    let language2 = 'नेपा'
    cy.get('.select').select(language1)
    cy.get('.select').should('have.value', 'en')
})

describe('Login Admin', () => {
    const apiUrl = Cypress.env('api_url');
    const invalidPMIS = 'asdfdsf'
    const wrongPassword = 'qqqqqq'
    let pmis, password;

    beforeEach('Visits Login Page', () => {
        cy.fixture('loginPMIS').then((data) => {
            pmis = data.email
            password = data.password
        })
        cy.visit('/login-admin')
        cy.wait(500)
        cy.nepToEngAdminLogin()
    })

    it('shows error when both input is empty', () => {
        cy.get('.btn').contains('Login').click()
        cy.get(':nth-child(1) > .error').should('have.text', '  Email is required')
        cy.get(':nth-child(2) > .error').should('have.text', '  Password is required')
    })

    it('shows error when pmisCode is empty', () => {
        cy.get('#password').type(wrongPassword)
        cy.get('.btn').contains('Login').click()
        cy.get('.error').should('have.text', '  Email is required')
    })

    it('shows error when password is empty', () => {
        cy.get('#username').type(invalidPMIS)
        cy.get('.btn').contains('Login').click()
        cy.get('.error').should('have.text', '  Password is required')
    })

    it('shows error on invalid credentials', () => {
        cy.get('#username').type(invalidPMIS)
        cy.get('#password').type(wrongPassword)
        cy.get('.btn').contains('Login').click()
        cy.get('.toast--message').should('have.text', 'Bad credentials')
    })

    it('verifies password show and hide icon', () => {
        cy.get('#password').type(wrongPassword)
        cy.get('.ic-hide').click()
        cy.get('.ic-hide').should('not.exist')
        cy.get('.ic-show').click()
        cy.get('.ic-show').should('not.exist')
    })

    it('logins on valid credentials', () => {
        cy.intercept('POST', apiUrl + '/login').as('login')
        cy.readFile("cypress/fixtures/loginPMIS.json", (err, data) => {
            if (err) {
                return console.error(err);
            };
        }).then(data => {
            cy.get('#username').type(data.pmis)
            cy.get('#password').type(data.password)
            cy.get('.btn-primary').click()
            cy.wait('@login')
            cy.get('.username').should('have.text', data.pmis)
        })
    })
})