describe('Password reset', () => {
        const apiUrl = Cypress.env('api_url');
        const serverId = 'zfpmgana'; // Replace SERVER_ID with an actual Mailosaur Server ID
        const testEmail = `sharad@${serverId}.mailosaur.net`
        const validPassword = 'Test@123'

        function En() {
            cy.get('.select').select('en');
        }

        it('Makes a Password Reset request', () => {
            cy.visit('/login')
            En()
            cy.intercept('POST', 'http://103.109.228.79:8070/external/user/forgot/password').as('forgotPassword')
            cy.contains('Forgot Password?').click()
            cy.get('#email').type(testEmail)
            cy.contains('Submit').click()
            cy.wait('@forgotPassword')
        })

        it('Gets a Password Reset email', () => {
            cy.mailosaurDeleteAllMessages(serverId)
            let password
            cy.mailosaurGetMessage(serverId, {
                sentTo: testEmail
            }).then(email => {
                expect(email.subject).to.equal('Password Reset Link');
                password = email.html.links[0].href
                cy.visit(password)
            })
            En()
            cy.get('#newPassword').type(validPassword)
            cy.get('#confrimPassword').type(validPassword)
            cy.get('.btn-primary').contains('Submit').click()
        })

        it('Logins with the user', () => {
            cy.intercept('POST', apiUrl + '/login').as('login')
            cy.get('#username').type(testEmail)
            cy.get('#password').type(validPassword)
            cy.get('.btn-primary').click()
            cy.wait('@login')
            cy.get('.username').should('have.text', testEmail)
        })
    })
    // it('Follows the link from the email', () => {
    //     const validPassword = 'delighted cheese jolly cloud'

//     cy.visit(passwordResetLink)
//     cy.title().should('contain', 'Change your password')
//     cy.get('#password').type(validPassword)
//     cy.get('#password_confirmation').type(validPassword)
//     cy.get('form').submit()
// })