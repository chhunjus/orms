describe('Change Password verification', () => {
    const apiUrl = Cypress.env('api_url');
    let email, password;
    const wrongPwd = 'Test!123'
    const invalidPwd = 'test!123'
    let randomString = Math.random().toString(36).substring(2, 7);
    const newPassword = "Test1" + randomString + "!"

    beforeEach('logins', () => {
        cy.fixture('login').then((data) => {
            email = data.email
            password = data.password
            cy.externalUserLogin(email, password)
        })
    })

    it('verifies change password function when data is empty', () => {
        cy.visit('/home')
        cy.get('.header > .list > .d-flex > .btn').then(($button) => {
            if ($button.text() === 'नेपा') {
                cy.wrap($button).click().then(() => {
                    expect($button.text()).to.eq('En')
                })
            }
        })
        cy.get('.username').click()
        cy.get('.dropdown-menu-right').contains('Change Password').click()

        //change password
        cy.get('button[type="submit"].btn').contains('Submit').click()
        cy.get('.form-group > .error').eq(0).should('have.text', '  Old Password is required')
        cy.get('.form-group > .error').eq(1).should('have.text', '  New Password is required')
        cy.get('.form-group > .error').eq(2).should('have.text', '  Confirm New Password is required')
    })

    it('verifies change password function when old password is empty', () => {
        cy.visit('/home')
        cy.get('.header > .list > .d-flex > .btn').then(($button) => {
            if ($button.text() === 'नेपा') {
                cy.wrap($button).click().then(() => {
                    expect($button.text()).to.eq('En')
                })
            }
        })
        cy.get('.username').click()
        cy.get('.dropdown-menu-right').contains('Change Password').click()

        //change password
        cy.get('#newPassword').type(wrongPwd)
        cy.get('#confrimPassword').type(wrongPwd)
        cy.get('button[type="submit"].btn').contains('Submit').click()
        cy.get('.form-group > .error').eq(0).should('have.text', '  Old Password is required')
    })

    it('verifies change password function when old password is invalid', () => {
        cy.visit('/home')
        cy.get('.header > .list > .d-flex > .btn').then(($button) => {
            if ($button.text() === 'नेपा') {
                cy.wrap($button).click().then(() => {
                    expect($button.text()).to.eq('En')
                })
            }
        })
        cy.get('.username').click()
        cy.get('.dropdown-menu-right').contains('Change Password').click()

        //change password
        cy.get('#oldPassword').type(invalidPwd)
        cy.get('#newPassword').type(password)
        cy.get('#confrimPassword').type(password)
        cy.get('button[type="submit"].btn').contains('Submit').click()
        cy.get('.toast--message').should('have.text', `Old password doesn't match !!`)
    })

    it('verifies change password function when confirm and new password doesnot match', () => {
        cy.visit('/home')
        cy.get('.header > .list > .d-flex > .btn').then(($button) => {
            if ($button.text() === 'नेपा') {
                cy.wrap($button).click().then(() => {
                    expect($button.text()).to.eq('En')
                })
            }
        })
        cy.get('.username').click()
        cy.get('.dropdown-menu-right').contains('Change Password').click()

        //change password
        cy.get('#oldPassword').type(password)
        cy.get('#newPassword').type(newPassword)
        cy.get('#confrimPassword').type(wrongPwd)
        cy.get('#newPassword').click()
        cy.get('.form-group > .error').eq(0).should('have.text', '  Confirm Password and New Password are not same')
    })

    it('verifies change password function when old and new password are same', () => {
        cy.visit('/home')
        cy.get('.header > .list > .d-flex > .btn').then(($button) => {
            if ($button.text() === 'नेपा') {
                cy.wrap($button).click().then(() => {
                    expect($button.text()).to.eq('En')
                })
            }
        })
        cy.get('.username').click()
        cy.get('.dropdown-menu-right').contains('Change Password').click()

        //change password
        cy.get('#oldPassword').type(password)
        cy.get('#newPassword').type(password)
        cy.get('#confrimPassword').click()
        cy.get('.form-group > .error').eq(0).should('have.text', '  New and Old Password are same')
    })

    //skipped bcuz in public portal, download api doesnot work
    it('verifies successful change password function', () => {
        cy.visit('/home')
        cy.get('.header > .list > .d-flex > .btn').then(($button) => {
            if ($button.text() === 'नेपा') {
                cy.wrap($button).click().then(() => {
                    expect($button.text()).to.eq('En')
                })
            }
        })
        cy.get('.username').click()
        cy.get('.dropdown-menu-right').contains('Change Password').click()

        //change password
        cy.intercept('POST', apiUrl + "/external/user/change/password").as('changePassword')
        cy.get('#oldPassword').type(password)
        cy.get('#newPassword').type(newPassword)
        cy.get('#confrimPassword').type(newPassword)
        cy.get('button[type="submit"].btn').contains('Submit').click()

        cy.readFile("cypress/fixtures/login.json", (err, data) => {
            if (err) {
                return console.error(err);
            };
        }).then((data) => {
            data.password = newPassword
            cy.writeFile("cypress/fixtures/login.json", JSON.stringify(data))
        })

        cy.wait('@changePassword').then(response => {
            expect(response.response.body).to.have.property('message')
            expect(response.response.body.message).to.contain("Password Successfully changed re-login with new password")
        })

        //login after password change
        cy.readFile("cypress/fixtures/login.json", (err, data) => {
            if (err) {
                return console.error(err);
            };
        }).then(data => {
            cy.externalUserLogin(data.email, data.password)
        })
        cy.visit('/home')
        cy.get('.username').should('have.text', email)
        cy.url().should('eq', Cypress.config().baseUrl + 'home')
    })


})