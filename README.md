# orms

'DashboardUI.spec.js', 'login.js' and 'Cypress.Commands{externalUserLogin}' are interconnected. When password is changed through 'dashboardUI.spec.js', password data is also updated in 'login.json' which affects 'Cypress.Commands{externalUserLogin}'.


New feature is added in ORMS where once vacancy is applied, the profile is locked and user cannot edit their profile, so new user login data must be used so that test won't fail.